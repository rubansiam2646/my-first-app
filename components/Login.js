import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Platform,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const login = ({navigation}) => {
  return (
    <KeyboardAwareScrollView>
    <View>
    <Icon
            name="arrow-back-outline"
            size={30}
            color="#4F8EF7"
            style={{marginLeft: 20,marginTop:20,}}
            onPress={() => navigation.navigate('Page')}
          />
      
      <View style={styles.welcome}>
        <Text style={styles.t_1}>welcome!</Text>
        <Text style={styles.t_2}>Sign in into continue</Text>
      </View>
      <View style={styles.forms}>
        <TextInput style={styles.input_1} placeholder="Email" />
        <TextInput
          style={styles.input_2}
          placeholder="password"
          underlineColorAndroid="transparent"
          secureTextEntry={true}
        />
      </View>
      <View style={styles.c_3}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Intro')} >
          <Text style={styles.b1}>log in</Text>
          
        </TouchableOpacity>
        <Text
          style={{
            textAlign: 'center',
            marginTop: 10,
            fontSize: 15,
            color: '#200561',
          }}>
          Forgot password?
        </Text>
      </View>
      <View>
        <Text style={styles.media}>Social media login</Text>
        <View
          style={{
            flexDirection: 'row',

            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 10,
          }}>
          <Icon name="logo-facebook" size={30} color="#4F8EF7" style={{marginRight:10,}} />
          <Icon name="logo-whatsapp" size={30} color="#228B22" />
          <Icon name="logo-instagram" size={30} color="#FF7F50" style={{marginLeft:10,}} />
        </View>
      </View>
      <View>
        <Text style={styles.last} onPress={() => navigation.navigate('Signup')}>
          Do you have any account signup?
        </Text>
      </View>
      
    </View>
    </KeyboardAwareScrollView>

  );
};

export default login;

const styles = StyleSheet.create({
  welcome: {
    marginTop: 60,
    marginLeft: 40,
  },
  t_1: {
    fontSize: 40,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: '#200561',
  },
  t_2: {
    fontSize: 20,
    letterSpacing: 1,
    color: '#200561',
    opacity: 0.5,
  },
  forms: {
    justifyContent: 'center',
    width: '70%',
    marginTop: 70,
    marginLeft: 40,
  },
  input_1: {
    fontSize: 20,
    borderBottomWidth: 1,
  },
  input_2: {
    fontSize: 20,
    borderBottomWidth: 1,
    marginTop: 40,
  },
  b1: {
    fontSize: 25,
    backgroundColor: 'blue',
    width: '60%',
    textAlign: 'center',
    marginLeft: 65,
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
    color: 'white',
    fontWeight: '600',
    letterSpacing: 1,
  },
  c_3: {
    marginTop: 70,
  },
  media: {
    textAlign: 'center',
    marginTop: 10,
    color: '#200561',
    fontSize: 20,
  },
  // icons:{
  //     flex: 1,
  //     flexDirection:'row',
  //     justifyContent:'center',
  //     marginTop:10,

  // },
  fb: {
    height: 30,
    width: 30,
    marginRight: 15,
  },
  ins: {
    height: 30,
    width: 30,
  },
  wp: {
    height: 30,
    width: 30,
    marginLeft: 15,
  },
  last: {
    marginTop: 30,
    textAlign: 'center',
    color: '#200561',
    fontSize: 15,
  },
});
